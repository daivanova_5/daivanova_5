import os

def ToMp4():
    for video in os.listdir("video"):
        if not (video.endswith(".mp4")):
            newFileName = video.split(".")[0] + ".mp4"
            os.system(f'ffmpeg -i video/{video} -c:v copy -c:a copy video/{newFileName}')
            os.remove(f'video/{video}')

def ToTs():
    for video in os.listdir("video"):
        newFileName = video.split(".")[0] + ".ts"
        os.system(f'ffmpeg -i video/{video} -c:v copy -c:a copy video/{newFileName}')
        os.remove(f'video/{video}')

def MakeAList(file):
    fileList = open(f'{file}.txt', 'w')
    for video in os.listdir('video'):
        fileList.write(f"file '{video}'\n")
    fileList.close()

def Splice(file):
    os.replace(f"{file}.txt", f"video/{file}.txt")
    os.chdir("video")
    fileList = open(f'{file}.txt', 'r')
    os.system(f'ffmpeg -f concat -i {file}.txt -c:v copy -c:a copy {file}.mp4')
    fileList.close()
    os.remove(f"{file}.txt")

def DelMoment(t1,t2,video1):
    os.mkdir("video")
    os.system(f"ffmpeg -t {t1} -i {video1} -c:v copy -c:a copy video/segment1.mp4")
    os.system(f"ffmpeg -ss {t2} -i {video1} -c:v copy -c:a copy video/segment2.mp4")
    MakeAList("segments")
    Splice("segments")
    os.chdir('..')
    os.remove(f"{video1}")
    os.replace("video/segments.mp4", f"{video1}")
    os.remove("video/segment1.mp4")
    os.remove("video/segment2.mp4")
    os.rmdir("video")

def DeleteAdvertisement():
    print("Enter the time frame of the ad, if there are no ads, click 0\nFormat 00:00:00 00:00:01.")
    os.chdir("video")
    for video in os.listdir():
        print(f"For {video} (if there are several fragments, enter their frames in the line):")
        timeList = list(input().split())
        if len(timeList) % 2 == 0 and len(timeList) != 0:
            while len(timeList) != 0:
                DelMoment(timeList[0], timeList[1], video)
                timeList = timeList[2:]
    os.chdir('..')

ToMp4()
DeleteAdvertisement()
ToTs()

MakeAList("videoo")
Splice("videoo")
